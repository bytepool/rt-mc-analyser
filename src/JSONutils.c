/*
  rt-mc-analyser - a package for analysing mixed-criticality systems
  utils.h - external definitions of helper functions
  Copyright (C) 2013 Aljoscha Lautenbach <aljoscha.lautenbach@gmail.com>
 
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <sys/types.h> /* open() */
#include <sys/stat.h> /* open() */
#include <fcntl.h> /* open() */
#include <errno.h> /* errno */

#include <stdio.h> /* printf */
#include <stdlib.h> /* exit(), qsort() */

#include "cJSON.h"
#include "utils.h"
#include "JSONutils.h"
#include "rt-mc.h"

/*
   Reads a bytestream from a file into a newly allocated string.

   The string needs to be freed by the caller.
 */
int read_file_as_string(char* filename, char** data)
{
    FILE* f = fopen(filename, "rb");

    if (f == NULL)
    {
	fprintf(stderr, "An error occurred trying to load from file: %s: ", filename);
	perror(NULL);
        exit(EXIT_FAILURE);
    }

    fseek(f, 0, SEEK_END);
    long len = ftell(f);
    fseek(f, 0, SEEK_SET);

    *data = (char*) malloc(len + 1);
    check_malloc(*data);

    fread(*data, 1, len, f);

    fclose(f);

    return TRUE;
}

/*
   Loads the given task with the parameters from the json object.
 */
int parse_task(cJSON* task_obj, Task* task)
{
    task->T = (cJSON_GetObjectItem(task_obj, T_JSON_LABEL))->valuedouble;
    task->D = (cJSON_GetObjectItem(task_obj, D_JSON_LABEL))->valuedouble;
    task->C_lo = (cJSON_GetObjectItem(task_obj, C_LO_JSON_LABEL))->valuedouble;
    task->C_hi = (cJSON_GetObjectItem(task_obj, C_HI_JSON_LABEL))->valuedouble;
    //task->U_lo = (cJSON_GetObjectItem(task_obj, U_LO_JSON_LABEL))->valuedouble;
    //task->U_hi = (cJSON_GetObjectItem(task_obj, U_HI_JSON_LABEL))->valuedouble;
    task->L = (cJSON_GetObjectItem(task_obj, L_JSON_LABEL))->valueint;
    task->assigned_processor = UNASSIGNED;

    task->U_lo = task->C_lo / task->T;
    task->U_hi = task->C_hi / task->T;

    return TRUE;
}

/*
   Loads a new task array with the tasks held in tasks_array.
 */
int load_tasks_from_json(int n, cJSON* tasks_array, Task** tasks)
{
    *tasks = malloc(n * sizeof(Task));
    check_malloc(*tasks);

    for (int i = 0; i < n; i++)
    {
	Task t;
	parse_task(cJSON_GetArrayItem(tasks_array, i), &t);
	(*tasks)[i] = t;
    }
    return TRUE;
}

/*
   Takes the given string and parses it into cJSON objects.
 */
int parse_json_string(char* json_string, Task** tasks, int* n)
{
    cJSON* root, *task_set_params, *tasks_array;

    root = cJSON_Parse(json_string);

    task_set_params = cJSON_GetObjectItem(root, TASK_SET_PARAMS_JSON_LABEL);
    tasks_array = cJSON_GetObjectItem(root, TASKS_JSON_LABEL);

    *n = (cJSON_GetObjectItem(task_set_params, NR_OF_TASKS_JSON_LABEL))->valueint;
    if (*n != cJSON_GetArraySize(tasks_array))
    {
	fprintf(stderr,
		"Error parsing JSON: Size of array does not match number in field %s. Aborting.\n",
		NR_OF_TASKS_JSON_LABEL);
	exit(EXIT_FAILURE);
    }

    load_tasks_from_json(*n, tasks_array, tasks);

    /* cJSON_Delete frees the entire structure, also all the contained objects
       so it is enough to delete the root object.  */
    cJSON_Delete(root);

    return TRUE;
}

/*
   Loads a taskset from a json file.

   Allocates new memory for the Task array. Needs to be freed by caller.
 */
int load_taskset_from_file(char* filename, Task** tasks, int* n)
{
    char* json_string = 0;

    read_file_as_string(filename, &json_string);

    parse_json_string(json_string, tasks, n);
    free(json_string);

    return TRUE;
}

