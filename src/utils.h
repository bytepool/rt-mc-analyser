/*
  rt-mc-analyser - a package for analysing mixed-criticality systems
  utils.h - external definitions of helper functions
  Copyright (C) 2013 Aljoscha Lautenbach <aljoscha.lautenbach@gmail.com>
 
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "rt-mc.h"

#ifndef UTILS_H
#define UTILS_H

#define FALSE 0
#define TRUE 1

/**
   Check whether the memory for the given pointer was properly allocated.
   If not, the program exits with an error message.
 */
void check_malloc(void* p);

/**
   Print a readable representation of a long double vector.
 */
void print_dvector(int, long double*);

/**
   Print a readable representation of an unsigned int vector.
 */
void print_ivector(int, unsigned int*);

/**
   Print a readable representation of all the parameters of a task.
 */
void print_tasks(int, Task*);

/**
   Print a readable representation of all the parameters of a task
   with high precision (10 decimal points for floats).
 */
void print_tasks_high_precision(int, Task*);

void print_task(Task*);
void print_task_high_precision(Task*);

void throw_error(const char* msg);

#endif /* UTILS_H */
