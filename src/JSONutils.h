/*
  rt-mc-analyser - a package for analysing mixed-criticality systems
  utils.h - external definitions of helper functions
  Copyright (C) 2013 Aljoscha Lautenbach <aljoscha.lautenbach@gmail.com>
 
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include "cJSON.h"
#include "rt-mc.h"

#ifndef JSON_UTILS_H
#define JSON_UTILS_H

int load_taskset_from_file(char* filename, /*out*/ Task** tasks, int* n);
int parse_json_string(char* json_string, /*out*/ Task** tasks, int* n);

int read_file_as_string(char* filename,/*out*/ char** data);
int load_tasks_from_json(int n, cJSON* tasks_array, Task** tasks);

int parse_task(cJSON* task_obj, Task* task);

#endif /* JSON_UTILS_H */
