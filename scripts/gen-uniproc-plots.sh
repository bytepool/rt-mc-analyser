#!/bin/bash
# simple script to generate the plots from the plot files in the gplot subfolder

set -e # stop when an error is encountered


results_dir=results/uniproc

for dir in ${results_dir}/T*; do
    dirname=${dir##*/}
    arg=$dirname
    T=$(echo $arg | cut -d'-' -f1)
    N=$(echo $arg | cut -d'-' -f2)
    CP=$(echo $arg | cut -d'-' -f3)
    CF=$(echo $arg | cut -d'-' -f4)
    D=$(echo $arg | cut -d'-' -f5)
    PS=$(echo $arg | cut -d'-' -f6)
    PE=$(echo $arg | cut -d'-' -f7)
    gnuplot -e "dirname='${dirname}';T='${T#T}';N='${N#N}';CP='${CP#CP}';CF='${CF#CF}';D='${D#D}';PS='${PS#PS}';PE='${PE#PE}'" gplot/U-uniproc.gplot
done

echo "All plots generated."
