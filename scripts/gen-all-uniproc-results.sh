#!/bin/bash
# simple script to test a number of tasksets

set -e # stop when an error is encountered

M=1

# in_pre = input prefix 
in_pre=tasksets/uniproc/
# out_pre = output prefix
out_pre=results/uniproc/

dirs=( Tlog-N10-CP0.5-CF2.0-Dconstr-PS10-PE1000  Tlog-N20-CP0.5-CF2.0-Dconstr-PS10-PE1000  Tlog-N40-CP0.5-CF2.0-Dconstr-PS10-PE1000 Tlog-N20-CP0.5-CF1.5-Dconstr-PS10-PE1000  Tlog-N20-CP0.7-CF2.0-Dconstr-PS10-PE1000 )
algos=( AMC IAMC UB_HL )
prios=( DM OPA )


for i in $(seq 0 $(( ${#dirs[@]} - 1 )) ); do
    for j in $(seq 0 $(( ${#algos[@]} - 1 )) ); do
	for k in $(seq 0 $(( ${#prios[@]} -1 )) ); do
       	    # fn = filename
	    in_dir=${in_pre}${dirs[$i]}
	    out_dir=${out_pre}${dirs[$i]}
	    fn=${algos[$j]}-${prios[$k]}

	    file_params="$in_dir $out_dir $fn"
	    params="${algos[$j]} ${prios[$k]}"

	    if [ ${algos[$j]} == "UB_HL" -a ${prios[$k]} == "OPA" ]; then continue; fi
	    scripts/gen-uniproc-results.sh $file_params $params &
	done
    done
    sleep 120 # wait x minutes to work on the next directory
done

echo "gen-all-uniproc-results: All jobs started."
