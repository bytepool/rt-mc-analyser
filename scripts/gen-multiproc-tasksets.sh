#!/bin/bash
# simple script to automatically create a new and complete set of tasksets

set -e # stop when an error is encountered

start_time=$(date +"%s")


M=16
N=$((M * 10)) # to have equal number of tasks per processor

# taskset parameters
Tlog=1
Dconstr=1
CP=0.5
CF=2.0
PS=1
PE=1000

if [ $Tlog -eq 1 ]; then
    Tlabel="log"
else
    Tlabel="unif"
fi

if [ $Dconstr -eq 1 ]; then
    Dlabel="constr"
else
    Dlabel="T"
fi

top_dir="tasksets/multiproc"
out_dir="${top_dir}/M${M}-T${Tlabel}-N${N}-CP${CP}-CF${CF}-D${Dlabel}-PS${PS}-PE${PE}"

taskgen=./build/rt-mc-taskgen


start=$((M * 25))
end=$((M * 1000))
step=$((M * 25))
U_values=$(seq $start $step $end)

for i in $U_values; do
    bc_prog="scale=3; ret = $i / 1000; if (ret < 1) { print 0; print ret} else {print ret}"
    U=$(echo $bc_prog | bc)
    dir_name=$out_dir/tasksets-U-$U

    if [ -d $dir_name ]; then
	rm -r $dir_name
    fi

    mkdir -p $dir_name
    
    for i in {1..1000}; do
	$taskgen $dir_name $i $U $Tlog $Dconstr $N $CP $CF $PS $PE
    done
done

end_time=$(date +"%s")
elapsed_time=$(( end_time - start_time ))

echo "It took $elapsed_time seconds to generate all tasksets."
