/*
  rt-mc-taskgen - generates task sets and writes it to a json file
  rt-mc-taskgen-conf.h - configuration parameters for rt-mc-taskgen
  Copyright (C) 2013 Aljoscha Lautenbach <aljoscha.lautenbach@gmail.com>
 
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef RT_MC_TASKGEN_CONF_H
#define RT_MC_TASKGEN_CONF_H

#define NR_OF_TASKS 20
#define NR_OF_PROCESSORS 1

#define CRITICALITY_PROBABILITY 0.5
#define CRITICALITY_FACTOR 2.0

#define CONSTRAINED_DEADLINES 1

#define PERIOD_START 1
#define PERIOD_END 1000

#define FILE_PREFIX "taskset-"
#define FILE_SUFFIX ".json"
#define FILE_DIR "tasksets"

#define MAX_FILE_COUNT 4294967295

/* 
   If LOG_PERIODS is 1, the task periods (T) will be distributed logarithmically, 
   otherwise they will be distributed uniformly.
*/
#define LOG_PERIODS 1
#define DEFAULT_UTILIZATION_FACTOR 0.8

#endif
