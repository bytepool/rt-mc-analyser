#!/bin/bash
# simple script to test a number of tasksets

set -e # stop when an error is encountered



# setting the correct M value is important for choosing the correct binary for the tests
#M=2
M=4
#M=8
#M=16

#N=$((M*3))
#N=$((M*5))
N=$((M*10))

dirs=( M${M}-Tlog-N${N}-CP0.5-CF2.0-Dconstr-PS1-PE1000 ) #M${M}-Tlog-N10-CP0.5-CF2.0-Dconstr-PS1-PE1000 M${M}-Tlog-N20-CP0.5-CF2.0-Dconstr-PS1-PE1000 )

# in_pre = input prefix 
in_pre=tasksets/multiproc/
# out_pre = output prefix
out_pre=results/multiproc/

algos=( IAMC ) # UB_HL AMC
prios=( OPA ) # DM
orderings=( CU ) # IU DU DM CM SM RAND CSM
allocations=( FF BF WF ) # 

for i in $(seq 0 $(( ${#dirs[@]} - 1 )) ); do
    for j in $(seq 0 $(( ${#algos[@]} - 1 )) ); do
	for k in $(seq 0 $(( ${#prios[@]} -1 )) ); do
	    for l in $(seq 0 $(( ${#orderings[@]} -1 )) ); do
		for m in $(seq 0 $(( ${#allocations[@]} -1 )) ); do
         	    # fn = filename
		    in_dir=${in_pre}${dirs[$i]}
		    out_dir=${out_pre}${dirs[$i]}
		    fn=${algos[$j]}-${prios[$k]}-${orderings[l]}-${allocations[m]}

		    file_params="$in_dir $out_dir $fn"
		    params="${algos[$j]} ${prios[$k]} ${orderings[l]} ${allocations[m]} $M"

		    if [ ${algos[$j]} == "UB_HL" -a ${prios[$k]} == "OPA" ]; then continue; fi
		    scripts/gen-multiproc-results.sh $file_params $params &
		done
	    done
	done
	#sleep 300
    done
    #sleep 120 # wait 5 minutes to work on the next directory
done

echo "gen-all-multiproc-results: All jobs started."
