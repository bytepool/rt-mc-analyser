CC=gcc
CFLAGS=-pedantic -Wall -g
ALL_CFLAGS=-std=c99 $(CFLAGS)

SRC_DIR=src
BUILD_DIR=build
LIBS=-lm
DEPS=$(SRC_DIR)/random.h $(SRC_DIR)/utils.h $(SRC_DIR)/cJSON.h $(SRC_DIR)/rt-mc-taskgen-conf.h $(SRC_DIR)/rt-mc.h $(SRC_DIR)/cJSON.h $(SRC_DIR)/JSONutils.h
OBJS=$(BUILD_DIR)/random.o $(BUILD_DIR)/utils.o $(BUILD_DIR)/cJSON.o $(BUILD_DIR)/JSONutils.o

MAN_DIR=man
logfile=comp-errors.log
log=2>&1 | awk '{print strftime() ": " $$0}' | tee -a $(logfile)


all: rt-mc-taskgen rt-mc-run

rt-mc-taskgen: $(SRC_DIR)/rt-mc-taskgen.c $(OBJS) $(DEPS) $(BUILD_DIR)/rt-mc-taskgen.o
	$(CC) $(ALL_CFLAGS) -o $(BUILD_DIR)/rt-mc-taskgen $(OBJS) $(BUILD_DIR)/rt-mc-taskgen.o $(LIBS) $(log)

rt-mc-run: $(SRC_DIR)/rt-mc-run.c $(OBJS) $(DEPS) $(BUILD_DIR)/rt-mc-run.o 
	$(CC) $(ALL_CFLAGS) -o $(BUILD_DIR)/rt-mc-run $(OBJS) $(BUILD_DIR)/rt-mc-run.o $(LIBS) $(log)

$(BUILD_DIR)/%.o: $(SRC_DIR)/%.c $(DEPS)
	$(CC) $(ALL_CFLAGS) -c -o $@ $< $(LIBS) $(log)

clean:
	rm -r $(BUILD_DIR)/* $(log)

docs: $(MAN_DIR)/rt-mc-taskgen.rst $(MAN_DIR)/rt-mc-run.rst
	rst2man $(MAN_DIR)/rt-mc-run.rst $(MAN_DIR)/rt-mc-run.1
	rst2man $(MAN_DIR)/rt-mc-taskgen.rst $(MAN_DIR)/rt-mc-taskgen.1

etags: $(SRC_DIR)/*
	etags src/*


