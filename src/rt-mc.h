/*
  rt-mc-analyser - a package for analysing mixed-criticality systems
  rt-mc.h - data structures and constants used in the entire package
  Copyright (C) 2013 Aljoscha Lautenbach <aljoscha.lautenbach@gmail.com>
 
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef RT_MC_H
#define RT_MC_H

#define T_JSON_LABEL "T"
#define D_JSON_LABEL "D"
#define C_LO_JSON_LABEL "C_lo"
#define C_HI_JSON_LABEL "C_hi"
#define U_LO_JSON_LABEL "U_lo"
#define U_HI_JSON_LABEL "U_hi"
#define L_JSON_LABEL "L"

#define NR_OF_TASKS_JSON_LABEL "NrOfTasks"
#define UTIL_FACTOR_JSON_LABEL "UtilizationFactor"
#define PERIOD_START_JSON_LABEL "PeriodStart"
#define PERIOD_END_JSON_LABEL "PeriodEnd"
#define CRIT_FACTOR_JSON_LABEL "CriticalityFactor"
#define CRIT_PROB_JSON_LABEL "CriticalityProbability"
#define LOG_PERIODS_LABEL "LogarithmicPeriods"
#define CONSTR_DEADLINES_LABEL "ConstrainedDeadlines"

#define TASK_SET_PARAMS_JSON_LABEL "TaskSetParameters"
#define TASKS_JSON_LABEL "Tasks"

#define UNASSIGNED -1


typedef enum { LOW, HIGH } criticality_t;

/*
  Possible priority assignment types:
  DM - deadline monotonic
  CM - criticality monotonic
  OPA - optimal prioriaty assignment (Audsley's algo)
  IU - increasing utilization
  DU - decreasing utilization
  SM - slack monotonic
 */
typedef enum { DM, CM, OPA, IU, DU, SM, CSM, CU, RAND } priority_t;

/* Analysis types: 
   NO_MC: no mixed criticality, just use C_lo values and perform standard RTA
   PC: partitioned criticality
   SMC_NO: Static Mixed Criticality, no runtime support (original Vestal)
   SMC: Static Mixed Criticality (adapted Vestal)
   AMC: Adaptive Mixed Criticality (Burns, Davis, Baruah)
   IAMC: Improved AMC (Pathan)
*/
typedef enum { NO_MC, PC, SMC_NO, SMC, AMC, IAMC, UB_HL } analysis_t;

typedef struct
{
    long double T; // period
    long double D; // deadline
    long double C_lo; // WCET (low criticality)
    long double C_hi; // WCET (high criticality)
    long double U_lo; // utilization (low criticality)
    long double U_hi; // utilization (high criticality)
    criticality_t L; // default criticality level
    int p; // priority
    char assigned_processor; // only used in multiprocessor mode
} Task;

#endif
