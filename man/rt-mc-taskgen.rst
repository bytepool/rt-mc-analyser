==============
 rt-mc-taskgen
==============

-------------------------------------------------------
automatic task generation of mixed criticality tasksets
-------------------------------------------------------

:Author: Aljoscha Lautenbach <aljoscha.lautenbach@gmail.com>
:Date:   2013-05-13
:Copyright: public domain
:Version: 0.1
:Manual section: 1
:Manual group: 

SYNOPSIS
========

  rt-mc-taskgen ?

DESCRIPTION
===========

Description of rt-mc-taskgen goes here, using reST.


OPTIONS
=======

--no-options            No options have been defined yet      
--help, -h              Show command line options and exit.

PROBLEMS
========

1. No known problems.

SEE ALSO
========

* rt-mc-run

BUGS
====

No known bugs.
