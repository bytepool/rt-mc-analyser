/*
  rt-mc-taskgen - generates task sets and writes it to a json file
  rt-mc-taskgen.c - main code for the task generation
  Copyright (C) 2013 Aljoscha Lautenbach <aljoscha.lautenbach@gmail.com>
 
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/*
  Notes:
  Until a proper configuration file and CLI option parsing has been implemented,
  all compile time configuration is done in rt-mc-taskgen-conf.h.
 */

#include <stdio.h> // printf
#include <stdlib.h> // exit(), atof()
#include <unistd.h> // access()
#include <math.h> // pow()
#include <string.h> // strlen(), strerror()

#include <sys/types.h> // open()
#include <sys/stat.h> // open()
#include <fcntl.h> // open()

#include <errno.h> // errno

#include "cJSON.h"

#include "rt-mc.h"
#include "rt-mc-taskgen-conf.h"
#include "utils.h"
#include "random.h"

#define VERBOSE 0

static long double* U_lo;  
static long double* T;
static long double* D;
static long double* C_lo;
static long double* C_hi;
static long double* U_hi;
static criticality_t* L;

static cJSON* root;
static Task* tasks;

struct Params {
    int nr_of_tasks;
    int period_start;
    int period_end;
    double util_factor;
    double crit_prob;
    double crit_factor;
    char log_periods;
    char constrained_deadlines;
};

/**
   Fills a Task array with the given values.
 */
void init_tasks(int n, long double* T, long double* D, long double* C_lo, long double* C_hi, 
		long double* U_lo, long double* U_hi, criticality_t* L, Task* tasks)
{
    for (int i = 0; i < n; i++)
    {
	tasks[i].T = T[i];
	tasks[i].D = D[i];
	tasks[i].C_lo = C_lo[i];
	tasks[i].C_hi = C_hi[i];
	tasks[i].U_lo = U_lo[i];
	tasks[i].U_hi = U_hi[i];
	tasks[i].L = L[i];
    }
}

/**
   uunifast - returns a n-size vector, where the total sum is equal to U.

   The returned vector is newly allocated and needs to be freed by the caller.
 */
long double* uunifast(int n, long double U) 
{
    long double * vectU = malloc(n * sizeof(long double));
    check_malloc(vectU);

    long double sumU = U;
    long double nextSumU;

    for (int i = 0; i < n - 1; i++)
    {
	nextSumU = sumU * pow(get_normalized_random(), 1.0 / (n - i));
	vectU[i] = sumU - nextSumU;
	sumU = nextSumU;
    }
    vectU[n - 1] = sumU;

    return vectU;
}

/**
   uunifast_discard - returns a n-size vector, where the total sum is equal to U.
                      The difference to uunifast is that uunifast_discard may have
		      a utilization value > 1, but if a single task has a utilization
		      value > 1, the whole taskset is discarded, and the algorithm begins
		      anew. 
		      This is needed for the multiprocessor case, since a task with 
		      utilization > 1 is unschedulable on any processor.

   The returned vector is newly allocated and needs to be freed by the caller.
 */
long double* uunifast_discard(int n, long double U)
{
    long double * vectU = malloc(n * sizeof(long double));
    check_malloc(vectU);

    long double sumU;
    long double nextSumU;
    char done = FALSE;
    
    while (!done)
    {
	done = TRUE;
	sumU = U;

	for (int i = 0; i < n - 1; i++)
	{
	    nextSumU = sumU * pow(get_normalized_random(), 1.0 / (n - i));
	    vectU[i] = sumU - nextSumU;
	    sumU = nextSumU;
	    if (vectU[i] > 1)
	    {
		done = FALSE;
		break;
	    }
	}
	vectU[n - 1] = sumU;
    }

    return vectU;
}


/**
   Generates n periods with a log-uniform distribution. 
   
   uniform_log = 1 / (x * (log(b) - log(a))), with a <= x <= b.
 */
long double* generate_log_periods(int n, double a, double b)
{
    double upper_limit = 1 / (log(b / a));
    int scale_factor = b * a;

    long double* periods = malloc(n * sizeof(long double));
    check_malloc(periods);

    double x;
    for (int i = 0; i < n; i++)
    { 
	x = get_normalized_random();
	x = a + (x * (b - a)); // a <= x <= b
	periods[i] = ((1 / (x * (log(b) - log(a)))) / upper_limit) * scale_factor;
    }
    return periods;
}

/**
   Generates n periods with a uniform distribution within the interval [a,b]. 
 */
long double* generate_uniform_periods(int n, double a, double b)
{
    long double* periods = malloc(n * sizeof(long double));
    check_malloc(periods);

    double x;
    for (int i = 0; i < n; i++)
    { 
	x = get_normalized_random();
	periods[i] = a + (x * (b - a));
    }
    return periods;
}

/**
   Generates n deadlines with a uniform distribution. 
   The interval depends on the criticality. For high criticality, the 
   interval is [C_hi, T], and for low criticality it is [C_lo, T].
 */
long double* generate_uniform_deadlines(int n, long double* T, long double* C_lo, 
					long double* C_hi, criticality_t* L)
{
    long double* deadlines = malloc(n * sizeof(long double));
    check_malloc(deadlines);

    double x;
    double a, b;
    for (int i = 0; i < n; i++)
    { 
	b = T[i];
	if (L[i] == LOW)
	    a = C_lo[i];
	else
	    a = C_hi[i];
	x = get_normalized_random();
	deadlines[i] = a + (x * (b - a));
    }
    return deadlines;
}


/**
   Calculate the worst-case execution times (WCETs) based on the given task
   periods (T) and utilizations (U).
 */
long double* calc_C_lo(int n, long double* T, long double* U)
{
    long double * C = malloc(n * sizeof(long double));
    check_malloc(C);

    for (int i = 0; i < n; i++)
    {
	C[i] = T[i] * U[i];
    }
    return C;
}

/**
   Calculates the WCETs for the HIGH criticality mode, based on the 
   WCETs for the LOW criticality mode and the given criticality_factor.
 */
long double* calc_C_hi(int n, long double* C_low, float criticality_factor)
{
    long double * C_high = malloc(n * sizeof(long double));
    check_malloc(C_high);

    for (int i = 0; i < n; i++)
    {
	C_high[i] = C_low[i] * criticality_factor;
    }
    return C_high;
}

/**
   Calculates the utilization values for the HIGH criticality mode, 
   based on the given periods and WCET values.
 */
long double * calc_U_hi(int n, long double* T, long double* C_high)
{
    long double * U_high = malloc(n * sizeof(long double));
    check_malloc(U_high);

    for (int i = 0; i < n; i++)
    {
	U_high[i] = C_high[i] / T[i];
    }

    return U_high;
}

/**
   Returns an array with n entries specifying either HIGH or LOW criticality.

   A random number between 0 and 1 is generated, and if it is lower than the 
   given probability value, it will be of LOW criticality, 
   otherwise it will be of HIGH criticality.
 */
criticality_t* determine_criticality(int n, float probability)
{
    criticality_t* L = malloc(n * sizeof(criticality_t));
    check_malloc(L);

    double random;

    for (int i = 0; i < n; i++)
    {
	random = get_normalized_random();
	if (random > probability)
	{
	    L[i] = LOW;
	}
	else
	{
	    L[i] = HIGH;
	}
    }
    return L;
}

/**
   Constructs a JSON array from the given task set.
 */
cJSON* create_json_task_array(int n, Task* tasks)
{
    cJSON* json_task_array;
    cJSON** task_parameters;

    task_parameters = malloc(n * sizeof(cJSON*));
    check_malloc(task_parameters);

    json_task_array = cJSON_CreateArray();

    for (int i = 0; i < n; i++)
    {
	task_parameters[i] = cJSON_CreateObject();

	cJSON_AddNumberToObject(task_parameters[i], T_JSON_LABEL, tasks[i].T);
	cJSON_AddNumberToObject(task_parameters[i], D_JSON_LABEL, tasks[i].D);
	cJSON_AddNumberToObject(task_parameters[i], C_LO_JSON_LABEL, tasks[i].C_lo);
	cJSON_AddNumberToObject(task_parameters[i], C_HI_JSON_LABEL, tasks[i].C_hi);
	//cJSON_AddNumberToObject(task_parameters[i], U_LO_JSON_LABEL, tasks[i].U_lo);
	//cJSON_AddNumberToObject(task_parameters[i], U_HI_JSON_LABEL, tasks[i].U_hi);
	cJSON_AddNumberToObject(task_parameters[i], L_JSON_LABEL, tasks[i].L);

	cJSON_AddItemToArray(json_task_array, task_parameters[i]);
    }

    return json_task_array;
}

/**
   Constructs a JSON root object with two toplevel objects, TaskSetParameters and Tasks.
   The first is an object with key-value pairs for all task set parameters, and the second
   is the list with the actual tasks for this task set.
 */
cJSON* create_json_root(int n, Task* tasks, struct Params* params)
{
    cJSON* root;
    cJSON *task_set_params, *tasks_array;

    root = cJSON_CreateObject();

    tasks_array = create_json_task_array(n, tasks);

    // construct task_set_params
    task_set_params = cJSON_CreateObject();
    cJSON_AddNumberToObject(task_set_params, NR_OF_TASKS_JSON_LABEL, params->nr_of_tasks);
    cJSON_AddNumberToObject(task_set_params, UTIL_FACTOR_JSON_LABEL, params->util_factor);
    cJSON_AddNumberToObject(task_set_params, PERIOD_START_JSON_LABEL, params->period_start);
    cJSON_AddNumberToObject(task_set_params, PERIOD_END_JSON_LABEL, params->period_end);
    cJSON_AddNumberToObject(task_set_params, CRIT_FACTOR_JSON_LABEL, params->crit_factor);
    cJSON_AddNumberToObject(task_set_params, CRIT_PROB_JSON_LABEL, params->crit_prob);
    cJSON_AddNumberToObject(task_set_params, LOG_PERIODS_LABEL, params->log_periods);
    cJSON_AddNumberToObject(task_set_params, CONSTR_DEADLINES_LABEL, params->constrained_deadlines);

    // add to root
    cJSON_AddItemToObject(root, TASK_SET_PARAMS_JSON_LABEL, task_set_params);
    cJSON_AddItemToObject(root, TASKS_JSON_LABEL, tasks_array);

    return root;
}

/**
   Tries to create the given directory on the file system.
   On success, it outputs a status message that the directory was created. 
   If the directory already existed, nothing happens.
 */
void create_subdir(char* subdir)
{
    // create parent directory subdir, if it does not exist
    if ( mkdir(subdir, 0777) == -1 )
    {
	if (errno != EEXIST)
	{
	    fprintf(stderr, "An error occured trying to create directory %s: ", subdir);
	    perror(NULL);
	    exit(EXIT_FAILURE);
	}
    }
    else
    {
	if (VERBOSE) printf("Created directory ./%s\n", subdir);
    }
}

/**
   Allocates memory for the full path of the json output file.
   The structure of the filename is as follows:
   ${subdir}/${prefix}${number}${suffix}

   count - The number to use for this taskset.

   The returned string needs to be freed by the caller!
 */
char * create_fullpath(int count, char* subdir, char* pre, char* suf)
{
    char * fn;
    int fn_len;

    // +2 for '/' and '\0', +10 for positions of int count (max 2^32 = 4294967296)
    fn_len = strlen(subdir) + 1 + strlen(pre) + 10 + strlen(suf) + 1;

    fn = malloc(fn_len); 
    check_malloc(fn);

    snprintf(fn, fn_len, "%s/%s%d%s", subdir, pre, count, suf);

    return fn;
}

/**
   Takes a JSON root object and writes it to the file as specified in the configuration by
   FILE_PREFIX, FILE_SUFFIX and FILE_DIR.

   A counter is created, and incremented until a file name is found which does not have a
   corresponding existing file yet.

   The maximum number of files to number is set in MAX_FILE_COUNT. If that maximum is reached,
   the program will abort without writing a file.
 */
void write_json_to_file(cJSON* root, char* subdir, char* prefix, char* suffix, int init_cnt)
{
    int count = init_cnt;

    char* fullpath;

    create_subdir(subdir);
    fullpath = create_fullpath(count, subdir, prefix, suffix);

    while (access(fullpath, F_OK ) != -1) // file exists
    {
	free(fullpath);

	++count;
	// construct new filename
	if (count >= MAX_FILE_COUNT)
	    throw_error("It seems that there are already 2^32 different files.");
	
	fullpath = create_fullpath(count, subdir, prefix, suffix);
    }

    if (VERBOSE) printf("Writing json'ized taskset to ./%s\n", fullpath);
    FILE* json_fp = fopen(fullpath, "w");

    if (json_fp == NULL)
    {
	int max_msg_len = 200;
        char * msg = malloc(max_msg_len * sizeof(char));
        snprintf(msg, max_msg_len, "An error occured trying to open %s.%s\n", 
		 fullpath, strerror(errno));
	throw_error(msg);
    }

    fprintf(json_fp, "%s\n", cJSON_Print(root));

    fclose(json_fp);
    free(fullpath);
}

/**
   Basic clean up operations such as freeing memory and closing file descriptors.
 */
void cleanup()
{
    cJSON_Delete(root);

    free(U_lo);
    free(U_hi);
    free(T);
    free(C_lo);
    free(C_hi);
    free(tasks);

    close_random_src();
}

int main(int argc, char * argv[]) 
{
    struct Params params;
    int file_nr = 1;

    // defined in rt-mc-taskgen-conf.h
    int n = NR_OF_TASKS; 
    int period_start = PERIOD_START;
    int period_end = PERIOD_END;

    double Uf = DEFAULT_UTILIZATION_FACTOR;
    double crit_prob = CRITICALITY_PROBABILITY;
    double crit_factor = CRITICALITY_FACTOR;

    char* subdir = NULL;
    char* prefix = FILE_PREFIX;
    char* suffix = FILE_SUFFIX;

    char constrained_deadlines = CONSTRAINED_DEADLINES;
    char log_periods = LOG_PERIODS;

    if (argc > 1)
    {
	subdir = argv[1];
    }
    if (argc > 2)
    {
	file_nr = atoi(argv[2]);
    }
    if (argc > 3)
    {
	Uf = atof(argv[3]);
    }
    if (argc > 4)
    {
	log_periods = atoi(argv[4]);
    }
    if (argc > 5)
    {
	constrained_deadlines = atoi(argv[5]);
    }
    if (argc > 6)
    {
	n = atoi(argv[6]);
    }
    if (argc > 7)
    {
	crit_prob = atof(argv[7]);
    }
    if (argc > 8)
    {
	crit_factor = atof(argv[8]);
    }
    if (argc > 9)
    {
	period_start = atof(argv[9]);
    }
    if (argc > 10)
    {
	period_end = atof(argv[10]);
    }

    if (subdir == NULL) subdir = FILE_DIR;

    params.nr_of_tasks = n;
    params.period_start = period_start;
    params.period_end = period_end;
    params.util_factor = Uf;
    params.crit_prob = crit_prob;
    params.crit_factor = crit_factor;
    params.log_periods = log_periods;
    params.constrained_deadlines = constrained_deadlines;

    init_random_src();

    if (Uf <= 1)
	U_lo = uunifast(n, Uf);
    else
	U_lo = uunifast_discard(n, Uf);
    if (log_periods)
    {
	T = generate_log_periods(n, period_start, period_end);
    }
    else
    {
	T = generate_uniform_periods(n, period_start, period_end);
    }
    C_lo = calc_C_lo(n, T, U_lo);
    C_hi = calc_C_hi(n, C_lo, crit_factor);
    U_hi = calc_U_hi(n, T, C_hi);
    L = determine_criticality(n, crit_prob);

    if (constrained_deadlines)
    {
	D = generate_uniform_deadlines(n, T, C_lo, C_hi, L);
    }
    else
    {
	D = T;
    }

    tasks = (Task*) malloc(n * sizeof(Task));
    check_malloc(tasks);

    init_tasks(n, T, D, C_lo, C_hi, U_lo, U_hi, L, tasks);

    root = create_json_root(n, tasks, &params);
    write_json_to_file(root, subdir, prefix, suffix, file_nr);

    cleanup();

    exit(EXIT_SUCCESS);
}
