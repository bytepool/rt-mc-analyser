#!/bin/bash
# simple script to generate the plots from the plot files in the gplot subfolder

set -e # stop when an error is encountered


results_dir=results/multiproc/M2*

method=( IAMC )
priority=( OPA DM )

cmp="ordering"
fitting=( FF BF WF )
ordering=( "*" )

for i in $(seq 0 $(( ${#fitting[@]} - 1 )) ); do
    for j in $(seq 0 $(( ${#method[@]} - 1 )) ); do
	for k in $(seq 0 $(( ${#priority[@]} - 1 )) ); do
	    for dir in ${results_dir}; do
		dirname=${dir##*/}
		arg=$dirname
		M=$(echo $arg | cut -d'-' -f1)
		T=$(echo $arg | cut -d'-' -f2)
		N=$(echo $arg | cut -d'-' -f3)
		CP=$(echo $arg | cut -d'-' -f4)
		CF=$(echo $arg | cut -d'-' -f5)
		D=$(echo $arg | cut -d'-' -f6)
		PS=$(echo $arg | cut -d'-' -f7)
		PE=$(echo $arg | cut -d'-' -f8)
		gnuplot -e "dirname='${dirname}';M='${M#M}';T='${T#T}';N='${N#N}';CP='${CP#CP}';CF='${CF#CF}';D='${D#D}';PS='${PS#PS}';PE='${PE#PE}';cmp='${cmp}';ordering='${ordering}';method='${method[j]}';priority='${priority[k]}';fitting='${fitting[i]}';" gplot/U-multiproc.gplot
	    done
	done
    done
done

cmp="fitting"
fitting=( "*" )
ordering=( DM DU IU CM SM CSM RAND CU )

for i in $(seq 0 $(( ${#ordering[@]} - 1 )) ); do
    for j in $(seq 0 $(( ${#method[@]} - 1 )) ); do
	for k in $(seq 0 $(( ${#priority[@]} - 1 )) ); do
	    for dir in ${results_dir}; do
		dirname=${dir##*/}
		arg=$dirname
		M=$(echo $arg | cut -d'-' -f1)
		T=$(echo $arg | cut -d'-' -f2)
		N=$(echo $arg | cut -d'-' -f3)
		CP=$(echo $arg | cut -d'-' -f4)
		CF=$(echo $arg | cut -d'-' -f5)
		D=$(echo $arg | cut -d'-' -f6)
		PS=$(echo $arg | cut -d'-' -f7)
		PE=$(echo $arg | cut -d'-' -f8)
		gnuplot -e "dirname='${dirname}';M='${M#M}';T='${T#T}';N='${N#N}';CP='${CP#CP}';CF='${CF#CF}';D='${D#D}';PS='${PS#PS}';PE='${PE#PE}';cmp='${cmp}';ordering='${ordering[i]}';method='${method[j]}';priority='${priority[k]}';fitting='${fitting}';" gplot/U-multiproc.gplot
	    done
	done
    done
done

echo "All plots generated."
