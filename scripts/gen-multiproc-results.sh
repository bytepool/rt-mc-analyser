#!/bin/bash
# simple script to test a number of tasksets

set -e # stop when an error is encountered

start_time=$(date +"%s")


# arg 1 = input directory
top_dir=${1:-tasksets/uniproc/tasksets-U-Tlog-Dconstr-N20-CP0.5-CF2.0}

# arg 2 = output directory
results_dir=${2:-results/uniproc/U-Tlog-Dconstr-N20-CP0.5-CF2.0}

# arg 3 = filename
filename=${3:-UB_HL-DM}
ext=dat

# arg 4 = algorithm
algo=${4:-UB_HL}

#arg 5 = priority assignment scheme
prio_assignment=${5:-DM}

#arg 6 = initial ordering
initial_order=${6:-IU}

#arg 7 = processor allocation strategy
alloc_strategy=${7:-FF}

#arg 8 = number of processors
M=${8:-1}


out_file="${results_dir}/${filename}.${ext}"
failed_tasksets="${results_dir}/failed.json"

scale=3
keep_failed=

declare -i total=0
declare -i schedulable=0


if [ ! -d $results_dir ]; then
    echo "Creating directory \"$results_dir\"..."
    mkdir -p $results_dir
fi

if [ -f $out_file ]; then
    echo "Removing existing file \"$out_file\"..."
    rm -r $out_file
fi

if [ -f $failed_tasksets ]; then
    echo "Removing existing file \"$failed_tasksets\"..."
    rm -r $failed_tasksets
fi


echo "Testing tasksets in \"${top_dir}\" with $algo using $prio_assignment with $initial_order and ${alloc_strategy}."
ran_test="$top_dir $algo $prio_assignment"

for dir in ${top_dir}/*; do
    total=0
    schedulable=0

    if [ -d $dir ]; then
	for taskset in ${dir}/*; do
	    total=$((total + 1))
	    
	    # run schedulability test for this taskset and count result.
	    # cut is needed because the first column is the name of the file that was tested.
	    res=$(./build/rt-mc-run-m${M} $taskset $algo $prio_assignment $initial_order $alloc_strategy | cut -f 2)

	    schedulable=$((schedulable + res))

	    # keep a record of the tasksets which were not schedulable
	    if [ $keep_failed ]; then
		if [ $res -eq 0 ]; then
		    cat $taskset >> $failed_tasksets
		fi
	    fi
	done

        # the utilization is part of the dir name, so split it off
	U=${dir#${top_dir}/tasksets-U-}
	percentage=$(echo "scale=$scale; $schedulable * 100 / $total" | bc)
	echo "$U $percentage" | tee -a $out_file
    else
	echo "\"$dir\" is not a directory, skipping it." 1>&2 
    fi
done

echo "Wrote results to \"$out_file\"."

end_time=$(date +"%s")
elapsed_time=$(( end_time - start_time ))

echo "$ran_test: It took $elapsed_time seconds to run all tests."
