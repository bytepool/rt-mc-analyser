/*
  rt-mc-analyser - a package for analysing mixed-criticality systems
  random.c - functions for accessing PRNGs
  Copyright (C) 2013 Aljoscha Lautenbach <aljoscha.lautenbach@gmail.com>
 
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <math.h>

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>

#include <limits.h>
#include <errno.h>


static int urand_fd;

/**
  Initializes the random source for use (i.e. opens /dev/urandom).
 */
void init_random_src()
{
    char * random_source = "/dev/urandom";

    urand_fd = open(random_source, O_RDONLY);
    if (urand_fd == -1)
    {
	fprintf(stderr, "An error occured trying to open %s. errno: %d\nExiting.\n", 
		random_source, errno);
	exit(EXIT_FAILURE);
    }
}

/**
  Closes this little helper library (i.e. closes /dev/urandom).
 */
void close_random_src()
{
    close(urand_fd);
}


/**
   Returns a normalized (from 0 to 1) random number, using /dev/urandom.
 */
long double get_normalized_random()
{
    // get random number
    unsigned short rand;
    read(urand_fd, &rand, sizeof rand);

    // normalize random number
    long double n_rand = ((long double)rand) / USHRT_MAX;

    return n_rand;
}

/**
  Allocates a long double vector of size n. Freeing the vector is responsibility of the caller.
 */
long double* get_random_vector(int n)
{
    long double * vector = malloc(n * sizeof(long double));
    long double n_rand;

    for (int i = 0; i < n; i++)
    {
	n_rand = get_normalized_random();
	vector[i] = n_rand;
    }

    return vector;
}

