/*
  rt-mc-run - a package for analysing mixed-criticality systems
  rt-mc-run.c - runs the analysis
  Copyright (C) 2013 Aljoscha Lautenbach <aljoscha.lautenbach@gmail.com>
 
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
*/


#include <stdio.h> /* printf */
#include <stdlib.h> /* exit(), qsort() */
#include <string.h> /* strcmp() */

#include <assert.h> /* assert() */
#include <errno.h> /* errno */

#include <math.h> /* pow() etc. */

#include "rt-mc.h"
#include "utils.h"
#include "JSONutils.h"

/* nr of processors */
#define NR_OF_PROCESSORS 1

/* verbosity of output: higher value = more detailed output */
#define VERBOSE 0

/* multiplication factor to reach higher precision in double comparison */
#define CMP_PRECISION 1000

/* status constansts */
#define UNSCHEDULABLE -2
#define NOT_APPLICABLE -3

/* Size of the static s array (see below). */
#define S_ARRAY_SIZE 10000

typedef struct {
    int proc_nr;
    Task* assigned_tasks;
    int task_count;
    size_t max_nr_of_tasks;
    long double U_lo;
    long double U_hi;
} Processor;

/* FF = first fit, BF = best fit, WF = worst fit */
typedef enum { FF, BF, WF} allocation_t;


/* module global variables */
static priority_t initial_ordering = RAND;
static allocation_t alloc_strategy = FF;

static analysis_t ana_method = IAMC;
static priority_t priority_assignment = DM;

static int n; /* nr of tasks in taskset */
static Task* tasks;
static Processor processors[NR_OF_PROCESSORS];

/* array holding possible criticality switch points (static for performance reasons) */
static long double s[S_ARRAY_SIZE];

/* default input file if none is given */
static char* input_file = "tasksets/taskset-1.json";


/* function prototypes */
char init_processors(Processor* procs, size_t max_size);
char add_task_to_processor(Processor* p, Task* task);
char remove_task_from_processor(Processor* p);

char first_fit(Task* task, int (*priority_order)(int n, Task* tasks), analysis_t method);
char best_fit(Task* task, int (*priority_order)(int n, Task* tasks), analysis_t method);
char worst_fit(Task* task, int (*priority_order)(int n, Task* tasks), analysis_t method);

int check_task_validity(Task* task);

int deadline_monotonic(int n, Task* tasks);
int criticality_monotonic(int n, Task* tasks);
int criticality_slack_monotonic(int n, Task* tasks);
int criticality_utilization_monotonic(int n, Task* tasks);
int slack_monotonic(int n, Task* tasks);
int opa_wrapper(int n, Task* tasks);

int increasing_utilization(int n, Task* tasks);
int decreasing_utilization(int n, Task* tasks);

int cmp_tasks_by_D (const void * a, const void * b);
int cmp_tasks_by_L_and_D (const void * a, const void * b);
int cmp_tasks_by_L_and_slack (const void * a, const void * b);
int cmp_tasks_by_L_and_U (const void * a, const void * b);
int cmp_tasks_by_inc_U_lo (const void * a, const void * b);
int cmp_tasks_by_dec_U_lo (const void * a, const void * b);

int cmp_procs_by_inc_U_lo (const void * a, const void * b);
int cmp_procs_by_dec_U_lo (const void * a, const void * b);

char optimal_priority_assignment(int n, Task* tasks, analysis_t method);
char opa_assign_lowest(int n, Task* tasks, int p, analysis_t method);

long double choose_C(Task* task, criticality_t* L, analysis_t method);
int higher_priority(int n, Task* task_set, Task* task, Task** hp_tasks, criticality_t* L, analysis_t method);

long double response_time_analysis(int n, Task* task_set, Task* task, criticality_t* L, analysis_t method);
long double calc_interference(long double R, int nr_tasks, Task** hp_tasks, criticality_t* L, analysis_t method);

int amc_nr_of_HI_releases(Task* task, long double s, long double t);
long double amc_criticality_switch_rt(int n, Task* tasks, Task* task, long double R_lo);

int possible_switch_points(int n, Task* tasks, Task* task, long double R_lo);

long double imp_amc_criticality_switch_rt(int n, Task* tasks, Task* task, long double R_lo);
long double imp_amc_interference_hi(Task* k, long double s, long double t, long double R_lo);
int imp_amc_nr_of_HI_releases(Task* k, long double s, long double t, long double R_lo);
int imp_amc_max_jobs_ref_pattern(Task* k, long double s, long double t, long double R_lo);
int imp_amc_max_jobs_in_interval(Task* k, long double s, long double t);

char adaptive_mixed_criticality(int n, Task* tasks);
char improved_adaptive_mixed_criticality(int n, Task* tasks);
char partitioned_criticality(int n, Task* tasks);

char schedulable(int n, Task* tasks, analysis_t method);

char parse_cli_args(int argc, char * argv[]);
void cleanup();


/*
  Initialize the processors by allocating memory for their tasksets
  and setting their variables to 0. 
*/
char init_processors(Processor* procs, size_t max_size)
{
    if (VERBOSE > 1) fprintf(stderr, "rt-mc-run: init_processors called.\n");

    for (int i = 0; i < NR_OF_PROCESSORS; i++)
    {
	procs[i].proc_nr = i;
	procs[i].max_nr_of_tasks = max_size;
	procs[i].task_count = 0;
	procs[i].U_lo = 0;
	procs[i].U_hi = 0;

	procs[i].assigned_tasks = malloc(sizeof(Task) * max_size);
	check_malloc(procs[i].assigned_tasks);
    }
    return TRUE;
}

/*
  Assign the given task to the given processor, if
  the maximum number of tasks will not be exceeded, 
  and the total processor utilization does not exceed 1.
*/
char add_task_to_processor(Processor* p, Task* task)
{
    if (VERBOSE > 1) fprintf(stderr, "rt-mc-run: add_task_to_processor called.\n");
    if (VERBOSE > 2) fprintf(stderr, "rt-mc-run: proc->U_lo: %LF.\n", p->U_lo);

    int cnt = p->task_count;

    if ( (cnt < p->max_nr_of_tasks) ) // sanity check
    {
	(p->assigned_tasks)[cnt] = *task;
	++(p->task_count);

	p->U_lo = p->U_lo + task->U_lo;
	p->U_hi = p->U_hi + task->U_hi;

	task->assigned_processor = p->proc_nr;
	if (VERBOSE > 1) fprintf(stderr, "rt-mc-run: added task to processor %d.\n", p->proc_nr);
	if (VERBOSE > 2) fprintf(stderr, "rt-mc-run: proc->U_lo: %LF.\n", p->U_lo);

	return TRUE;
    }
    return FALSE;
}

char remove_task_from_processor(Processor* p)
{
    if (VERBOSE > 2) fprintf(stderr, "rt-mc-run: remove_task_from_processor called.\n");
    if (VERBOSE > 2) fprintf(stderr, "rt-mc-run: proc->U_lo: %LF.\n", p->U_lo);

    --(p->task_count);
    Task* task = &((p->assigned_tasks)[p->task_count]);

    task->assigned_processor = UNASSIGNED;

    p->U_lo = p->U_lo - task->U_lo;
    p->U_hi = p->U_hi - task->U_hi;

    if (p->task_count < 0)
	throw_error("remove_task_from_processor: task_count < 0");

    if (VERBOSE > 2) fprintf(stderr, "rt-mc-run: removed task from processor %d.\n", p->proc_nr);
    if (VERBOSE > 2) fprintf(stderr, "rt-mc-run: proc->U_lo: %LF.\n", p->U_lo);

    return TRUE;
}

/*
  Basic cleanup function (freeing memory, etc.).  
*/
void cleanup()
{
    free(tasks);
}


/*
  Compares two given Tasks by their deadlines (D values).  

  Used by qsort for comparison. Precise to the log10(CMP_PRECISION) 
  decimal point. 
*/
int cmp_tasks_by_D (const void * a, const void * b)
{
    Task* x = (Task*)a;
    Task* y = (Task*)b;

    /* multiplying by CMP_PRECISION helps to differentiate decimal differences, 
       otherwise 19.9 and 20.1 might end up wrongly sorted for instance. */
    return ( (x->D * CMP_PRECISION) - (y->D * CMP_PRECISION) );
}

/*
  Compares two given Tasks by their low utilization values.

  Used by qsort for comparison. Precise to the log10(CMP_PRECISION) 
  decimal point. 
*/
int cmp_tasks_by_inc_U_lo (const void * a, const void * b)
{
    Task* x = (Task*)a;
    Task* y = (Task*)b;

    /* multiplying by CMP_PRECISION helps to differentiate decimal differences, 
       otherwise 19.9 and 20.1 might end up wrongly sorted for instance. */
    return ( (x->U_lo * CMP_PRECISION) - (y->U_lo * CMP_PRECISION) );
}

/*
  Compares two given Tasks by their low utilization values.
*/
int cmp_tasks_by_dec_U_lo (const void * a, const void * b)
{
    Task* x = (Task*)a;
    Task* y = (Task*)b;

    /* multiplying by CMP_PRECISION helps to differentiate decimal differences, 
       otherwise 19.9 and 20.1 might end up wrongly sorted for instance. */
    return ( (y->U_lo * CMP_PRECISION) - (x->U_lo * CMP_PRECISION) );
}

/*
  Compares two given Tasks by their slack. Slack is defined as follows:
  S = C - D
*/
int cmp_tasks_by_slack (const void * a, const void * b)
{
    Task* x = (Task*)a;
    Task* y = (Task*)b;

    long double slackX = (x->D * CMP_PRECISION) - (x->C_lo * CMP_PRECISION);
    long double slackY = (y->D * CMP_PRECISION) - (y->C_lo * CMP_PRECISION);

    return ( slackX - slackY );
}

/*
  Compares two given Processors by their low utilization values.

  Used by qsort for comparison. Precise to the log10(CMP_PRECISION) 
  decimal point. 
*/
int cmp_procs_by_inc_U_lo (const void * a, const void * b)
{
    Processor* x = (Processor*)a;
    Processor* y = (Processor*)b;

    /* multiplying by CMP_PRECISION helps to differentiate decimal differences, 
       otherwise 19.9 and 20.1 might end up wrongly sorted for instance. */
    return ( (x->U_lo * CMP_PRECISION) - (y->U_lo * CMP_PRECISION) );
}

/*
  Compares two given Processors by their low utilization values.

  Used by qsort for comparison. Precise to the log10(CMP_PRECISION) 
  decimal point. 
*/
int cmp_procs_by_dec_U_lo (const void * a, const void * b)
{
    Processor* x = (Processor*)a;
    Processor* y = (Processor*)b;

    /* multiplying by CMP_PRECISION helps to differentiate decimal differences, 
       otherwise 19.9 and 20.1 might end up wrongly sorted for instance. */
    return ( (y->U_lo * CMP_PRECISION) - (x->U_lo * CMP_PRECISION) );
}

/*
  Assign priorities to the tasks in the given task set according to
  Deadline Monotonic (DM) priority assignment.

  Sorts the tasks array according to RM as side effect.
*/
int deadline_monotonic(int n, Task* tasks)
{
    qsort(tasks, n, sizeof(Task), cmp_tasks_by_D);

    for (int i = 0; i < n; i++)
    {
	tasks[i].p = i + 1;
    }
    return 1;
}

/*
*/
int increasing_utilization(int n, Task* tasks)
{
    qsort(tasks, n, sizeof(Task), cmp_tasks_by_inc_U_lo);
    return 1;
}

/*
*/
int decreasing_utilization(int n, Task* tasks)
{
    qsort(tasks, n, sizeof(Task), cmp_tasks_by_dec_U_lo);
    return 1;
}

/*
  Compares two given Tasks by their criticality levels (L values).  

  Used by qsort for comparison. Precise to the log10(CMP_PRECISION) 
  decimal point. 
*/
int cmp_tasks_by_L_and_D (const void * a, const void * b)
{
    Task* x = (Task*)a;
    Task* y = (Task*)b;

    if (x->L > y->L)
	return -1;
    else if (x->L < y->L)
	return 1;
    else if (x->L == y->L)
    {
	/* multiplying by CMP_PRECISION helps to differentiate decimal differences, 
	   otherwise 19.9 and 20.1 might end up wrongly sorted for instance. */
	return ( (x->D * CMP_PRECISION) - (y->D * CMP_PRECISION) );
    }
    return 0;
}

/*
  Compare by criticality level, and if equal, compare by decreasing utilization.
 */
int cmp_tasks_by_L_and_U (const void * a, const void * b)
{
    Task* x = (Task*)a;
    Task* y = (Task*)b;

    if (x->L > y->L)
	return -1;
    else if (x->L < y->L)
	return 1;
    else if (x->L == y->L)
    {
	return ( (y->U_lo * CMP_PRECISION) - (x->U_lo * CMP_PRECISION) );
    }
    return 0;
}


int cmp_tasks_by_L_and_slack (const void * a, const void * b)
{
    Task* x = (Task*)a;
    Task* y = (Task*)b;

    if (x->L > y->L)
	return -1;
    else if (x->L < y->L)
	return 1;
    else if (x->L == y->L)
    {
	long double slackX = (x->D * CMP_PRECISION) - (x->C_lo * CMP_PRECISION);
	long double slackY = (y->D * CMP_PRECISION) - (y->C_lo * CMP_PRECISION);

	/* multiplying by CMP_PRECISION helps to differentiate decimal differences, 
	   otherwise 19.9 and 20.1 might end up wrongly sorted for instance. */
	return ( slackX - slackY );
    }
    return 0;
}

/*
  Assign priorities to the tasks in the given task set according to
  Deadline Monotonic (DM) priority assignment.

  Sorts the tasks array according to RM as side effect.
*/
int criticality_monotonic(int n, Task* tasks)
{
    qsort(tasks, n, sizeof(Task), cmp_tasks_by_L_and_D);

    for (int i = 0; i < n; i++)
    {
	tasks[i].p = i + 1;
    }
    return 1;
}

int criticality_slack_monotonic(int n, Task* tasks)
{
    qsort(tasks, n, sizeof(Task), cmp_tasks_by_L_and_slack);

    for (int i = 0; i < n; i++)
    {
	tasks[i].p = i + 1;
    }
    return 1;
}

int criticality_utilization_monotonic(int n, Task* tasks)
{
    qsort(tasks, n, sizeof(Task), cmp_tasks_by_L_and_U);

    for (int i = 0; i < n; i++)
    {
	tasks[i].p = i + 1;
    }
    return 1;
}

int slack_monotonic(int n, Task* tasks)
{
    qsort(tasks, n, sizeof(Task), cmp_tasks_by_slack);

    for (int i = 0; i < n; i++)
    {
	tasks[i].p = i + 1;
    }
    return 1;
}

/*
  Returns true if any task in the given task set with unassigned priority can 
  be assigned the lowest priority, false otherwise. 
  It is a helper function for optimal priority assignment (OPA).  

  n - number of tasks in task set
  tasks - array containing the task set
  assigned - the nr of tasks which have already been assigned. priority = n - assigned
*/
char opa_assign_lowest(int n, Task* tasks, int assigned, analysis_t method)
{
    if (VERBOSE > 1) printf("opa_assign_lowest called: assigned=%d\n", assigned);
    long double rta;

    for (int i = 0; i < n; i++)
    {
	if (VERBOSE > 2) printf("opa_assign_lowest: i=%d\n", i);

        /* skip already assigned tasks, unassigned tasks have negative priority */
	if (tasks[i].p >= 0) 
	{
	    if (VERBOSE > 3) printf("opa_assign_lowest: skipping task %d: t[%d].p=%d\n", i, i, tasks[i].p);
	    continue; 
	}

	/* try to assign lowest available priority */
	tasks[i].p = n - assigned;

	if (method == AMC)
	{
	    rta = adaptive_mixed_criticality(n, tasks);
	    if (!rta) rta = UNSCHEDULABLE;
	}
	else if (method == IAMC)
	{
	    rta = improved_adaptive_mixed_criticality(n, tasks);
	    if (!rta) rta = UNSCHEDULABLE;
	}
	else
	{
	    rta = response_time_analysis(n, tasks, &tasks[i], NULL, method);
	}

	if (VERBOSE > 1) printf("opa_assign_lowest: rta=%.6LF\n", rta);

	if (rta == UNSCHEDULABLE) 
	{
	    tasks[i].p = UNASSIGNED; /* make sure task is set back to unassigned */
	}
	else
	{
	    return TRUE;
	}
    }
    return FALSE;
}

/*
  Assign priorities to the tasks in the given task set according to
  Audsley's optimal priority assignment.

  The criticality level L is needed for the call to opa_assign_lowest, 
  because it uses standard response time analysis to determine if a 
  task is schedulable with the lowest priority or not.
  And the RTA function asks for the criticality level 
*/
char optimal_priority_assignment(int n, Task* tasks, analysis_t test)
{
    int assigned = 0;

    /* initialize p with an invalid value to know which tasks have not been assigned yet
       p <= n must hold.  */
    for (int i = 0; i < n; i++)
    {
	tasks[i].p = UNASSIGNED;
    }

    while(opa_assign_lowest(n, tasks, assigned, test) != FALSE) assigned++;

    if (VERBOSE > 1) printf("OPA: assigned=%d\n", assigned);

    if (assigned == n)
	return TRUE;
    else 
	return FALSE;
}


/*
  Find the set of tasks with higher priority than the given task
  in the given task set for the targeted criticality level L.

  The given task array hp_tasks needs to be pre-allocated and big enough 
  to contain at least n task pointers. 
  It will contain all higher priority tasks on exit. 

  If the given criticality pointer L is NULL, criticality is not considered. 
  If criticality is LOW, all tasks are considered.
  If criticality is HIGH, only high criticality tasks are considered.
  This is consistent with the workings of AMC.

  Returns the number of higher priority tasks.
*/
int higher_priority(int n, Task* task_set, Task* task, Task** hp_tasks, criticality_t* L, analysis_t method)
{
    int nr_hp_tasks = 0;

    switch (method)
    {
    case IAMC:
    case UB_HL:
    case AMC:
	for (int i = 0; i < n; i++)
	{
	    if (VERBOSE > 2) printf("higher_prio: cur_task.p=%d, task[%d].p=%d, %d<%d?\n", 
				    task->p, i, task_set[i].p, task_set[i].p, task->p);

	    if (task_set[i].p < task->p)
	    { /* all tasks are considered if L is NULL, 
		 but only tasks with criticality matching L are considered otherwise.  */
		if ( (L == NULL) || (*L == task_set[i].L))
		{
		    hp_tasks[nr_hp_tasks] = &task_set[i];
		    ++nr_hp_tasks;
		}
	    }
	}
	break;
    case PC:
    case NO_MC:
	/* only consider priority, but ignore criticality */
	for (int i = 0; i < n; i++)
	{
	    if (VERBOSE > 2) printf("higher_prio: cur_task.p=%d, task[%d].p=%d, %d<%d?\n", 
				    task->p, i, task_set[i].p, task_set[i].p, task->p);

	    if (task_set[i].p < task->p)
	    { 
		hp_tasks[nr_hp_tasks] = &task_set[i];
		++nr_hp_tasks;
	    }
	}
	break;
    default:
	throw_error("Unknown analysis type.");
    }
    return nr_hp_tasks;
}


/*
  Caclulates the interference, based on the given higher priority tasks
  current response time R, and targeted criticality level L. 

  If criticality is NULL or LOW, C_lo is used for all tasks, 
  otherwise C_hi is used.
*/
long double calc_interference(long double R, int nr_tasks, Task** hp_tasks, criticality_t* L, analysis_t method)
{
    long double C;
    long double sum = 0;

    for (int i = 0; i < nr_tasks; i++)
    {
	C = choose_C(hp_tasks[i], L, method);
	sum = sum + ceill(R / hp_tasks[i]->T) * C;
    }
    return sum;
}


/*
  Perform a number of tests to see if the task is valid under the current assumptions.

  If a task is not valid, the program aborts with an error message, 
  otherwise TRUE is returned.
*/
int check_task_validity(Task* task)
{
    char valid = 1;
    char* err_msg;

    /* Check for WCET with a zero value, and abort if one is found. */
    if (task->C_lo == 0 || task->C_hi == 0)
    {
	valid = 0;
	err_msg = "WCET of current task is 0!";
    }
    else if (task->C_lo > task->C_hi)
    {
	valid = 0;
	err_msg = "C_lo bigger than C-hi";
    }
    else if (task->D > task->T)
    {
	valid = 0;
	err_msg = "Deadline D bigger than perdiod T.";
    }

    if (!valid)
    {
	int max_msg_len = 40;
	char * msg = malloc(max_msg_len * sizeof(char));
	snprintf(msg, max_msg_len, "Invalid task encountered.\n%s\n", err_msg);
	throw_error(msg);
    }
    return TRUE;
}

/*
  Based on the criticality level and the analysis type, 
  choose the correct WCET.
*/
long double choose_C(Task* task, criticality_t* L, analysis_t method)
{
    long double C = 0;
    switch (method)
    {
    case UB_HL:
    case IAMC:
    case AMC:
	/* L can be NULL, e.g. during OPA */
	if ((L == NULL) || (*L == LOW))
	{
	    C = task->C_lo;
	}
	else if (*L == HIGH)
	{
	    C = task->C_hi;
	}
	break;

    case PC:
	if (task->L == LOW)
	{
	    C = task->C_lo;
	}
	else if (task->L == HIGH)
	{
	    C = task->C_hi;
	}
	break;
	
    case NO_MC:
	C = task->C_lo;
	break;

    case SMC_NO:
    case SMC:
    default:
	throw_error("Unknown analysis type.");
    }
    return C;
}

/*
  In the paper this function is called M(k, s, t)
*/
int amc_nr_of_HI_releases(Task* task, long double s, long double t)
{
    int val = ceill((t - s - (task->T - task->D)) / task->T) + 1;
    int max = ceill(t / task->T);
    
    return val < max ? val : max;
}

/*
  Determine the possible criticality switch points. 
  This function stores the s values in the static array s. 
  Old values are overwritten. 
  Returns the number of switch points found. 

  Constraints for s:
  - s < R_lo
  - task_s.p < task_i.p (priority of s must be higher (lower value) than i)
  - task_s.L == LOW
  - s must coincide with release of a low criticality task. 
*/
int possible_switch_points(int n, Task* tasks, Task* task, long double R_lo)
{
    int cnt = 0;
    long double r = 0; 
    long double r_prev = -1;

    for (int i = 0; i < n; i++)
    {
	if (tasks[i].T == 0)
	{
	    throw_error("Error: T = 0 for some task.\n");
	}
	if (((R_lo / tasks[i].T) + 1) >= S_ARRAY_SIZE)
	{
	    int max_msg_len = 120;
	    char * msg = malloc(max_msg_len * sizeof(char));
	    snprintf(msg, max_msg_len, "Nr of criticality switch points exceeds S_ARRAY_SIZE (%d). Recompile with higher value.\n", S_ARRAY_SIZE);
	    throw_error(msg);
	}

	/* in this context, tasks[i] = task_s */
	if ((tasks[i].L == LOW) && (tasks[i].p <= task->p))
	{
	    r = 0;
	    while ( r < R_lo)
	    {
                /* simple optimization to avoid lots of recomputations (mostly for r = 0) */
		if (r_prev != r) 
		{
		    s[cnt++] = r;
		    r_prev = r;
		}
		r += tasks[i].T;
	    }
	}
    }
    return cnt;
}

/*
  Response time analysis for the third AMC option, i.e. during the criticality switch. 
*/
long double amc_criticality_switch_rt(int n, Task* tasks, Task* task, long double R_lo)
{
    int M, len_s, nr_hp_lo, nr_hp_hi;
    Task **hp_lo, **hp_hi;
    criticality_t L;

    long double I_lo, I_hi;

    long double C = task->C_hi;
    long double R = C;
    long double R_max = R;
    
    len_s = possible_switch_points(n, tasks, task, R_lo);

    hp_lo = malloc(n * sizeof(Task*));
    check_malloc(hp_lo);
    L = LOW;
    nr_hp_lo = higher_priority(n, tasks, task, hp_lo, &L, AMC);

    hp_hi = malloc(n * sizeof(Task*));
    check_malloc(hp_hi);
    L = HIGH;
    nr_hp_hi = higher_priority(n, tasks, task, hp_hi, &L, AMC);

    for (int i = 0; i < len_s; i++)
    {
	I_lo = 0;
	for (int j = 0; j < nr_hp_lo; j++)
	{
	    I_lo = I_lo + (floorl(s[i] / hp_lo[j]->T) + 1) * hp_lo[j]->C_lo;
	}

	I_hi = 0;
	for (int k = 0; k < nr_hp_hi; k++)
	{
	    M = amc_nr_of_HI_releases(hp_hi[k], s[i], R);
	    I_hi = I_hi + M * hp_hi[k]->C_hi + (ceill(R / hp_hi[k]->T) - M) * hp_hi[k]->C_lo;
	}

	R = C + I_lo + I_hi;

	if (R > R_max)
	    R_max = R;
    }
    
    free(hp_lo);
    free(hp_hi);

    if (R_max > task->D)
	return UNSCHEDULABLE;
    else
	return R_max;
}

/*
  Performs a response time analysis for the given task in the given task set
  for the targeted criticality level L. 

  If L is NULL, criticality is ignored (C_lo is used), otherwise the matching
  WCET time is used. 

  Returns the response time of the given task within the given task set
  if it is schedulable, UNSCHEDULABLE if it is not schedulable, and NOT_APPLICABLE if the
  analysis is not applicable (e.g. because the given task has low criticality, 
  but L is HIGH.
*/
long double response_time_analysis(int n, Task* task_set, Task* task, criticality_t* L, analysis_t method)
{
    long double R, R_prev;
    int nr_hp_tasks;
    Task** hp_tasks;
    long double C = 0;
    long double interference = 0;

    // TODO: comment back in. But right now the tasksets have some C = 0.
    //check_task_validity(task);

    /* in high criticality mode, low criticality tasks are not considered */
    if (L != NULL && *L == HIGH && task->L == LOW)
    {
	return NOT_APPLICABLE; 
    }

    hp_tasks = malloc(n * sizeof(Task*));
    check_malloc(hp_tasks);

    C = choose_C(task, L, method);

    R = R_prev = C;

    if ((method == AMC || method == IAMC || method == UB_HL) && L != NULL && *L == LOW)
	nr_hp_tasks = higher_priority(n, task_set, task, hp_tasks, NULL, method);
    else
	nr_hp_tasks = higher_priority(n, task_set, task, hp_tasks, L, method);

    if (VERBOSE > 1) printf("response_time_analysis: nr_hp_tasks=%i\n", nr_hp_tasks);

    while (1)
    {
	if(nr_hp_tasks)
	    interference = calc_interference(R, nr_hp_tasks, hp_tasks, L, method);

	R = C + interference;

	if (R > task->D)
	{
	    free(hp_tasks);
	    return UNSCHEDULABLE;
	}
	if (R == R_prev)
	{
	    free(hp_tasks);
	    return R;
	}
	R_prev = R;
    }
}


/*
  In the paper this function is called N(k, s, t)
*/
int imp_amc_max_jobs_in_interval(Task* k, long double s, long double t)
{
    long double numerator = t - s - k->C_hi;
    if (numerator < 0) numerator = 0;
    int N = ceill(numerator / k->T);
    
    return N;
}

/*
  In the paper this function is called M^(k, s, t)
*/
int imp_amc_max_jobs_ref_pattern(Task* k, long double s, long double t, long double R_lo)
{
    int N = imp_amc_max_jobs_in_interval(k, s, t);

    if ((t - k->C_hi - (N * k->T) + R_lo) < s)
	return N;
    else
	return N + 1;
}

/*
  In the paper this function is called M(k, s, t)
*/
int imp_amc_nr_of_HI_releases(Task* k, long double s, long double t, long double R_lo)
{
    int N = imp_amc_max_jobs_in_interval(k, s, t);
    int M_caret = imp_amc_max_jobs_ref_pattern(k, s, t, R_lo);
    int max_releases = ceill(t / k->T);

    long double nu = t - k->C_hi - (N * k->T) + R_lo;

    /* alpha is both beta and gamma, depending on the value of nu */
    long double alpha = s - t + k->C_hi + (N * k->T) - R_lo;

    if (nu >= s)
	alpha += k->T;

    if ((alpha <= k->C_hi) && (M_caret < max_releases))
	return M_caret + 1;
    else
	return M_caret;
}


long double imp_amc_interference_hi(Task* k, long double s, long double t, long double R_lo)
{
    if (s <= k->D)
    {
	return ceill(t / k->T) * k->C_hi;
    }
    else
    {
	int M = imp_amc_nr_of_HI_releases(k, s, t, R_lo);
	return (M * k->C_hi + ( ceill(t / k->T) - M ) * k->C_lo);
    }
}

/*
 */
long double imp_amc_criticality_switch_rt(int n, Task* tasks, Task* task, long double R_lo)
{
    int len_s, nr_hp_lo, nr_hp_hi;
    Task **hp_lo, **hp_hi;
    criticality_t L;

    long double I_lo, I_hi;

    long double C = task->C_hi;
    long double R = C;
    long double R_max = R;
    
    len_s = possible_switch_points(n, tasks, task, R_lo);

    hp_lo = malloc(n * sizeof(Task*));
    check_malloc(hp_lo);
    L = LOW;
    nr_hp_lo = higher_priority(n, tasks, task, hp_lo, &L, AMC);

    hp_hi = malloc(n * sizeof(Task*));
    check_malloc(hp_hi);
    L = HIGH;
    nr_hp_hi = higher_priority(n, tasks, task, hp_hi, &L, AMC);

    for (int i = 0; i < len_s; i++)
    {
	I_lo = 0;
	for (int j = 0; j < nr_hp_lo; j++)
	{
	    long double val = hp_lo[j]->C_lo;
	    long double val2 = s[i] - (floorl(s[i] / hp_lo[j]->T) * hp_lo[j]->T);
	    if ( val2 < val)
		val = val2;
	    
	    I_lo += ( floorl(s[i] / hp_lo[j]->T) * hp_lo[j]->C_lo ) + val;
	}

	if (I_lo > s[i])
	    I_lo = s[i];


	I_hi = 0;
	for (int k = 0; k < nr_hp_hi; k++)
	{
	    /* use an upper bound, so that OPA can work */
	    long double R_lo = hp_hi[k]->D - (hp_hi[k]->C_hi - hp_hi[k]->C_lo);
	    I_hi += imp_amc_interference_hi(hp_hi[k], s[i], R, R_lo);
	}

	R = C + I_lo + I_hi;

	if (R > R_max)
	    R_max = R;
    }
    
    free(hp_lo);
    free(hp_hi);

    if (R_max > task->D)
	return UNSCHEDULABLE;
    else
	return R_max;
}

/*
  Risat's improvements on AMC.

  n - number of tasks in task set
  tasks - array with task set
*/
char improved_adaptive_mixed_criticality(int n, Task* tasks)
{
    analysis_t method = IAMC;
    criticality_t L;
    long double R, R_lo, R_hi;
    Task * task;

    for (int i = 0; i < n; i++)
    {
	task = &tasks[i];

	if (task->L == LOW)
	{
	    L = LOW;
	    R_lo = response_time_analysis(n, tasks, task, &L, method);
	    if (VERBOSE > 1) printf("IAMC: L=LOW; task %i, rta=%LF\n", i, R_lo);

	    if (R_lo == UNSCHEDULABLE)
		return FALSE;
	}
	else
	{
	    L = LOW;
	    R_lo = response_time_analysis(n, tasks, task, &L, method);
	    if (VERBOSE > 1) printf("IAMC: L=LOW; task %i, rta=%LF\n", i, R_lo);

	    if (R_lo == UNSCHEDULABLE)
		return FALSE;

	    L = HIGH;
	    R_hi = response_time_analysis(n, tasks, task, &L, method);
	    if (VERBOSE > 1) printf("IAMC: L=HIGH; task %i, rta=%LF\n", i, R_hi);

	    if (R_hi == UNSCHEDULABLE)
		return FALSE;

	    R = imp_amc_criticality_switch_rt(n, tasks, task, R_lo);
	    if (VERBOSE > 1) printf("IAMC: Switch; task %i, rta=%LF\n", i, R);

	    if (R == UNSCHEDULABLE)
		return FALSE;
	}
    }
    return TRUE;
}

char adaptive_mixed_criticality(int n, Task* tasks)
{
    analysis_t method = AMC;
    criticality_t L;
    long double R, R_lo, R_hi;
    Task * task;

    for (int i = 0; i < n; i++)
    {
	task = &tasks[i];

	if (task->L == LOW)
	{
	    L = LOW;
	    R_lo = response_time_analysis(n, tasks, task, &L, method);
	    if (VERBOSE > 1) printf("AMC: L=LOW; task %i, rta=%LF\n", i, R_lo);

	    if (R_lo == UNSCHEDULABLE)
		return FALSE;
	}
	else
	{
	    L = LOW;
	    R_lo = response_time_analysis(n, tasks, task, &L, method);
	    if (VERBOSE > 1) printf("AMC: L=LOW; task %i, rta=%LF\n", i, R_lo);

	    if (R_lo == UNSCHEDULABLE)
		return FALSE;

	    L = HIGH;
	    R_hi = response_time_analysis(n, tasks, task, &L, method);
	    if (VERBOSE > 1) printf("AMC: L=HIGH; task %i, rta=%LF\n", i, R_hi);

	    if (R_hi == UNSCHEDULABLE)
		return FALSE;

	    R = amc_criticality_switch_rt(n, tasks, task, R_lo);
	    if (VERBOSE > 1) printf("AMC: Switch; task %i, rta=%LF\n", i, R);

	    if (R == UNSCHEDULABLE)
		return FALSE;
	}
    }
    return TRUE;
}


/*
  Run RTA for partitioned criticality, according to Davis's paper.
*/
char partitioned_criticality(int n, Task* tasks)
{
    analysis_t method = PC;
    long double R;
    for (int i = 0; i < n; i++)
    {
	R = response_time_analysis(n, tasks, &tasks[i], NULL, method);
	if (VERBOSE > 1) printf("schedulable: %i, rta=%LF\n", i, R);
	if (R == UNSCHEDULABLE) return FALSE;
    }
    return TRUE;
}

/*
  Run standard RTA, only considering the low criticality tasks.
*/
char no_mixed_criticality(int n, Task* tasks)
{
    analysis_t method = NO_MC;
    long double R;
    for (int i = 0; i < n; i++)
    {
	R = response_time_analysis(n, tasks, &tasks[i], NULL, method);
	if (VERBOSE > 1) printf("NO_MC: schedulable: %i, rta=%LF\n", i, R);
	if (R == UNSCHEDULABLE) return FALSE;
    }
    return TRUE;
}

/*
  Run 
*/
char upper_bound(int n, Task* tasks)
{
    analysis_t method = UB_HL;
    long double R_lo, R_hi;
    Task * task;
    criticality_t L;

    for (int i = 0; i < n; i++)
    {
	task = &tasks[i];

	L = LOW;
	R_lo = response_time_analysis(n, tasks, task, &L, method);
	if (VERBOSE > 1) printf("UB-H&L: L=LOW; task %i, rta=%LF\n", i, R_lo);

	if (R_lo == UNSCHEDULABLE)
	    return FALSE;

	L = HIGH;
	R_hi = response_time_analysis(n, tasks, task, &L, method);
	if (VERBOSE > 1) printf("UB-H&L: L=HIGH; task %i, rta=%LF\n", i, R_hi);

	if (R_hi == UNSCHEDULABLE)
	    return FALSE;
    }
    return TRUE;
}

int opa_wrapper(int n, Task* tasks)
{
    return optimal_priority_assignment(n, tasks, ana_method);
}

/*
  Returns TRUE if the given task set is schedulable under RTA, 
  FALSE otherwise.
*/
char schedulable(int n, Task* tasks, analysis_t method)
{
    char sched;

    switch (method)
    {
    case NO_MC:
	sched = no_mixed_criticality(n, tasks);
	break;
    case PC:
	sched = partitioned_criticality(n, tasks);
	break;
    case AMC:
	sched = adaptive_mixed_criticality(n, tasks);
	break;
    case IAMC:
	sched = improved_adaptive_mixed_criticality(n, tasks);
	break;
    case UB_HL:
	sched = upper_bound(n, tasks);
	break;
    case SMC_NO:
    case SMC:
    default:
	throw_error("Unknown analysis type.");
    }
	
    return sched;
}

/*
  Parse command line arguments, using fixed positions.

  argv[1] is the name of the input file.
  argv[2] is the analysis type.
  argv[3] is the priority assignment type.
*/
char parse_cli_args(int argc, char * argv[])
{
    if (argc > 1)
    {
	input_file = argv[1];
    }
    if (argc > 2)
    {
	if (strcmp(argv[2], "AMC") == 0)
	    ana_method = AMC;
	else if (strcmp(argv[2], "IAMC") == 0)
	    ana_method = IAMC;
	else if (strcmp(argv[2], "UB_HL") == 0)
	    ana_method = UB_HL;
	else if (strcmp(argv[2], "NO_MC") == 0)
	    ana_method = NO_MC;
	else if (strcmp(argv[2], "PC") == 0)
	    ana_method = PC;
	else
	    throw_error("Unknown analysis type argument specified on command line.");
    }
    if (argc > 3)
    {
	if (strcmp(argv[3], "OPA") == 0)
	    priority_assignment = OPA;
	else if (strcmp(argv[3], "DM") == 0)
	    priority_assignment = DM;
	else if (strcmp(argv[3], "CM") == 0)
	    priority_assignment = CM;
	else
	    throw_error("Unknown priority assignment argument specified on command line.");
    }
    if (argc > 4)
    {
	if (strcmp(argv[4], "IU") == 0)
	    initial_ordering = IU;
	else if (strcmp(argv[4], "DU") == 0)
	    initial_ordering = DU;
	else if (strcmp(argv[4], "DM") == 0)
	    initial_ordering = DM;
	else if (strcmp(argv[4], "CM") == 0)
	    initial_ordering = CM;
	else if (strcmp(argv[4], "CU") == 0)
	    initial_ordering = CU;
	else if (strcmp(argv[4], "SM") == 0)
	    initial_ordering = SM;
	else if (strcmp(argv[4], "CSM") == 0)
	    initial_ordering = CSM;
	else if (strcmp(argv[4], "RAND") == 0)
	    initial_ordering = RAND;
	else
	    throw_error("Unknown initial ordering argument specified on command line.");
    }
    if (argc > 5)
    {
	if (strcmp(argv[5], "FF") == 0)
	    alloc_strategy = FF;
	else if (strcmp(argv[5], "BF") == 0)
	    alloc_strategy = BF;
	else if (strcmp(argv[5], "WF") == 0)
	    alloc_strategy = WF;
	else
	    throw_error("Unknown allocation strategy argument specified on command line.");
    }
    return TRUE;
}

char first_fit(Task* task, int (*priority_order)(int n, Task* tasks), analysis_t method)
{
    Processor* proc;
    char sched = 0;
    for (int i = 0; i < NR_OF_PROCESSORS; i++)
    {
	proc = &(processors[i]);

	if (VERBOSE > 0) fprintf(stderr, "first_fit: testing processor %d.\n", proc->proc_nr);

	add_task_to_processor(proc, task);
	task->assigned_processor = proc->proc_nr;

	// enforce priority order on processor
	sched = priority_order(proc->task_count, proc->assigned_tasks);

	if (VERBOSE > 2) fprintf(stderr, "tasks on proc %d after sorting:\n", proc->proc_nr);
	if (VERBOSE > 2) print_tasks_high_precision(proc->task_count, proc->assigned_tasks);

	// run chosen test
	if (priority_assignment != OPA)
	    sched = schedulable(proc->task_count, proc->assigned_tasks, method);

	if (sched)
	{
	    return TRUE;
	}
	else
	{
	    task->assigned_processor = UNASSIGNED;
	    remove_task_from_processor(proc);
	}
    }
    return FALSE;
}

char best_fit(Task* task, int (*priority_order)(int n, Task* tasks), analysis_t method)
{
    Processor* proc;
    char sched = 0;
    qsort(processors, NR_OF_PROCESSORS, sizeof(Processor), cmp_procs_by_dec_U_lo);

    for (int i = 0; i < NR_OF_PROCESSORS; i++)
    {
	proc = &(processors[i]);

	if (VERBOSE > 0) fprintf(stderr, "best_fit: testing processor %d.\n", proc->proc_nr);

	add_task_to_processor(proc, task);
	task->assigned_processor = proc->proc_nr;

	// enforce priority order on processor
	sched = priority_order(proc->task_count, proc->assigned_tasks);

	if (VERBOSE > 2) fprintf(stderr, "tasks on proc %d after sorting:\n", proc->proc_nr);
	if (VERBOSE > 2) print_tasks_high_precision(proc->task_count, proc->assigned_tasks);

	// run chosen test
	if (priority_assignment != OPA)
	    sched = schedulable(proc->task_count, proc->assigned_tasks, method);

	if (sched)
	{
	    return TRUE;
	}
	else
	{
	    task->assigned_processor = UNASSIGNED;
	    remove_task_from_processor(proc);
	}
    }
    return FALSE;
}

char worst_fit(Task* task, int (*priority_order)(int n, Task* tasks), analysis_t method)
{
    Processor* proc;
    char sched = 0;

    qsort(processors, NR_OF_PROCESSORS, sizeof(Processor), cmp_procs_by_inc_U_lo);

    for (int i = 0; i < NR_OF_PROCESSORS; i++)
    {
	proc = &(processors[i]);

	if (VERBOSE > 0) fprintf(stderr, "worst_fit: testing processor %d.\n", proc->proc_nr);

	add_task_to_processor(proc, task);
	task->assigned_processor = proc->proc_nr;

	// enforce priority order on processor
	sched = priority_order(proc->task_count, proc->assigned_tasks);

	if (VERBOSE > 2) fprintf(stderr, "tasks on proc %d after sorting:\n", proc->proc_nr);
	if (VERBOSE > 2) print_tasks_high_precision(proc->task_count, proc->assigned_tasks);

	// run chosen test
	if (priority_assignment != OPA)
	    sched = schedulable(proc->task_count, proc->assigned_tasks, method);

	if (sched)
	{
	    return TRUE;
	}
	else
	{
	    task->assigned_processor = UNASSIGNED;
	    remove_task_from_processor(proc);
	}
    }
    return FALSE;
}

/*
  Returns TRUE if the given task set is schedulable, 
  FALSE otherwise.
*/
char multiproc_schedulable(int n, Task* tasks, analysis_t method)
{
    Task* cur_task;
    char assigned;
    int nr_assigned_tasks = 0;

    /* function pointers */
    char (*alloc_function)(Task*, int (*priority_order)(int n, Task* tasks), analysis_t method) = NULL;
    int (*priority_order)(int n, Task* tasks) = NULL;

    // pre-sort tasks
    switch (initial_ordering)
    {
    case DM:
	deadline_monotonic(n, tasks);
	break;
    case CM:
	criticality_monotonic(n, tasks);
	break;
    case CSM:
	criticality_slack_monotonic(n, tasks);
	break;
    case CU:
	criticality_utilization_monotonic(n, tasks);
	break;
    case IU:
	increasing_utilization(n, tasks);
	break;
    case DU:
	decreasing_utilization(n, tasks);
	break;
    case SM:
	slack_monotonic(n, tasks);
	break;
    case RAND:
	break;
    default:
	throw_error("multiproc_schedulable: Chosen initial ordering is not implemented yet!");
    }

    // selection strategy
    switch (alloc_strategy)
    {
    case FF:
	alloc_function = &first_fit;
	break;
    case BF:
	alloc_function = &best_fit;
	break;
    case WF:
	alloc_function = &worst_fit;
	break;
    default:
	throw_error("multiproc_schedulable: Chosen allocation strategy is not implemented yet!");
    }

    // priority order
    switch (priority_assignment)
    {
    case DM:
	priority_order = &deadline_monotonic;
	break;
    case CM:
	priority_order = &criticality_monotonic;
	break;
    case OPA:
	priority_order = &opa_wrapper;
	break;
    default:
	throw_error("multiproc_schedulable: Chosen priority order is not implemented yet!");
    }

    while (nr_assigned_tasks < n)
    {
	// choose next task
	for (int i = 0; i < n; i++)
	{
	    if (tasks[i].assigned_processor == UNASSIGNED)
	    {
		if (VERBOSE) fprintf(stderr, "task %d unassigned, testing now.\n", i);
		cur_task = &(tasks[i]);

		// see if current task fits with given priority order and test on any processor
		assigned = alloc_function(cur_task, priority_order, method);
		
		if (assigned)
		{
		    ++nr_assigned_tasks;
		    if (VERBOSE > 2) fprintf(stderr, "task %d is schedulable on processor %d\n", i, tasks[i].assigned_processor);
		    break; // break out of for loop, hand back to while loop
		}
		else
		{
		    if (VERBOSE > 2) fprintf(stderr, "task %d is NOT schedulable on any processor.\n", i);
		    return FALSE;
		}
	    }
	}
    }
    return TRUE;
}

int main(int argc, char * argv[]) 
{
    char sched;

    atexit(cleanup);

    parse_cli_args(argc, argv);

    /* read json file to get task set */
    load_taskset_from_file(input_file, &tasks, &n);

    if (NR_OF_PROCESSORS > 1)
    {
	init_processors(processors, (size_t) n);

	sched = multiproc_schedulable(n, tasks, ana_method);
    }
    else /* uniproc */
    {
	switch (priority_assignment)
	{
	case OPA:
	    sched = optimal_priority_assignment(n, tasks, ana_method);
	    break;
	case DM:
	    deadline_monotonic(n, tasks); 
	    sched = schedulable(n, tasks, ana_method);
	    break;
	case CM:
	    criticality_monotonic(n, tasks); 
	    sched = schedulable(n, tasks, ana_method);
	    break;
	default:
	    throw_error("Given priority assignment not implemented yet!");
	}
    }

    /* printing the task set so late to make sure the priority ordering has happened */
    if (VERBOSE) print_tasks_high_precision(n, tasks);

    if (sched)
	printf("%s:\t1\n", input_file);
    else
	printf("%s:\t0\n", input_file);

    exit(EXIT_SUCCESS);
}
