==========
 rt-mc-run
==========

---------------------------------------------------
run a number of tests of mixed criticality tasksets
---------------------------------------------------

:Author: Aljoscha Lautenbach <aljoscha.lautenbach@gmail.com>
:Date:   2013-05-13
:Copyright: public domain
:Version: 0.1
:Manual section: 1
:Manual group: 

SYNOPSIS
========

  rt-mc-run input_file

DESCRIPTION
===========

  Description of rt-mc-run goes here, in reST format.

OPTIONS
=======

--help, -h              Show command line options and exit.

PROBLEMS
========

1. No known problems.

SEE ALSO
========

* rt-mc-taskgen

BUGS
====

No known bugs.

