/*
  rt-mc-analyser - a package for analysing mixed-criticality systems
  help.c - helper functions, e.g. error checking etc.
  Copyright (C) 2013 Aljoscha Lautenbach <aljoscha.lautenbach@gmail.com>
 
  This program is free software; you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation; either version 2 of the License, or
  (at your option) any later version.
 
  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.
 
  You should have received a copy of the GNU General Public License along
  with this program; if not, write to the Free Software Foundation, Inc.,
  51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#include <stdio.h>
#include <stdlib.h>
#include <errno.h>

#include "utils.h"

/*
  Print the given message to stderr and abort program.
 */
void throw_error(const char* msg)
{
    fprintf(stderr, "Error: %s\n", msg);
    exit(EXIT_FAILURE);
}

/**
   Checks the given pointer for NULL.
   If it is NULL, an error message is printed, and the program aborts.
 */
void check_malloc(void* p)
{
    if (p == NULL)
    {
	perror("An error occured trying to allocate memory with malloc");
	exit(EXIT_FAILURE);
    }
}

/**
   Print a readable representation of a long double vector.
 */
void print_dvector(int n, long double* v)
{
    int i;
    printf("[");
    for (i = 0; i < n; i++)
    {
	if (i != n - 1)
	    printf("%.2LF, ", v[i]);
	else
	    printf("%.2LF]\n", v[i]);
    }
}

/**
   Print a readable representation of an unsigned int vector.
 */
void print_ivector(int n, unsigned int* v)
{
    int i;
    printf("[");
    for (i = 0; i < n; i++)
    {
	if (i != n - 1)
	    printf("%u, ", v[i]);
	else
	    printf("%u]\n", v[i]);
    }
}

void print_tasks(int n, Task* tasks)
{
    for (int i = 0; i < n; i++)
    {
	printf("%i:\t[T:\t%.2LF,\tD:\t%.2LF,\tC_low:\t%.2LF,\tC_high:\t%.2LF,\tU_low:\t%.2LF,\tU_high:\t%.2LF,\tL:\t%i,\tp:\t%d]\n", i+1, 
	       tasks[i].T, tasks[i].D, tasks[i].C_lo, tasks[i].C_hi, tasks[i].U_lo, 
	       tasks[i].U_hi, (int)(tasks[i].L), tasks[i].p);
    }
}

void print_tasks_high_precision(int n, Task* tasks)
{
    for (int i = 0; i < n; i++)
    {
	printf("%i:\t[T:\t%.10LF,\tD:\t%.10LF,\tC_low:\t%.10LF,\tC_high:\t%.10LF,\tU_low:\t%.10LF,\tU_high:\t%.10LF,\tL:\t%i,\tp:\t%d]\n", i+1, 
	       tasks[i].T, tasks[i].D, tasks[i].C_lo, tasks[i].C_hi, tasks[i].U_lo, 
	       tasks[i].U_hi, (int)(tasks[i].L), tasks[i].p);
    }
}

void print_task(Task* task)
{
    printf("[T:\t%.2LF,\tD:\t%.2LF,\tC_low:\t%.2LF,\tC_high:\t%.2LF,\tU_low:\t%.2LF,\tU_high:\t%.2LF,\tL:\t%i,\tp:\t%d]\n",
	   task->T, task->D, task->C_lo, task->C_hi, task->U_lo, task->U_hi, 
	   (int)(task->L), task->p);
}

void print_task_high_precision(Task* task)
{
    printf("[T:\t%.10LF,\tD:\t%.10LF,\tC_low:\t%.10LF,\tC_high:\t%.10LF,\tU_low:\t%.10LF,\tU_high:\t%.10LF,\tL:\t%i,\tp:\t%d]\n",
	   task->T, task->D, task->C_lo, task->C_hi, task->U_lo, task->U_hi, 
	   (int)(task->L), task->p);

}

